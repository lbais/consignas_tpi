#include "sistema.h"

Sistema::Sistema() {
}

Sistema::Sistema(const Campo &c, const Secuencia<Drone> &ds) {
}

const Campo &Sistema::campo() const {
    return Campo();
}

EstadoCultivo Sistema::estadoDelCultivo(const Posicion &p) const {
    return EstadoCultivo();
}

const Secuencia<Drone> &Sistema::enjambreDrones() const {
    return Secuencia<Drone>();
}

void Sistema::crecer() {
}

void Sistema::seVinoLaMaleza(const Secuencia<Posicion> &ps) {
}

void Sistema::seExpandePlaga() {
}

void Sistema::despegar(const Drone &d) {
}

bool Sistema::listoParaCosechar() const {
    return false;
}

void Sistema::aterrizarYCargarBaterias(Carga b) {
}

void Sistema::fertilizarPorFilas() {
}

void Sistema::volarYSensar(const Drone &d) {
}

void Sistema::mostrar(std::ostream &os) const {
}

void Sistema::guardar(std::ostream &os) const {
}

void Sistema::cargar(std::istream &is) {
}

bool Sistema::operator==(const Sistema &otroSistema) const {
    return false;
}

std::ostream &operator<<(std::ostream &os, const Sistema &s) {
    return os;
}